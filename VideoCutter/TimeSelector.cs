﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoCutter
{
    public partial class TimeSelector : UserControl
    {
        public event Action<TimeSpan> ValueChanged;
        public DateTime max;
        public TimeSelector()
        {
            InitializeComponent();
            this.Time = AppVars.emptyTimeSpan;
        }

        public TimeSpan Time
        {
            get
            {
                return new TimeSpan(0, Convert.ToInt32(numHour.Value), Convert.ToInt32(numMin.Value), Convert.ToInt32(numSec.Value), Convert.ToInt32(numMilliSeconds.Value));
            }
            set
            {
                setMaxTime(value);
                numHour.Value = value.Hours;
                numMin.Value = value.Minutes;
                numSec.Value = value.Seconds;
                numMilliSeconds.Value = value.Milliseconds;
            }
        }
        private static DateTime GetDate(TimeSpan span)
        {
            return new DateTime(2000, 1, 1, span.Hours, span.Minutes, span.Seconds,span.Milliseconds);
        }
        private void dtpTime_ValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null) ValueChanged.Invoke(this.Time);
        }

        private void numMilliSeconds_ValueChanged(object sender, EventArgs e)
        {
            if (GetDate(this.Time) > max)
                numMilliSeconds.Value = max.Millisecond;
            if (ValueChanged != null) ValueChanged.Invoke(this.Time);
        }

        public void setMaxTime(TimeSpan time)
        {
            max = GetDate(time);
            numHour.Value = max.Hour;
            numMin.Value = max.Minute;
            numSec.Value = max.Second;
        }

    }
}
