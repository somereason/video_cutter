﻿namespace VideoCutter
{
    partial class TimeSelector
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.numMilliSeconds = new System.Windows.Forms.NumericUpDown();
            this.numHour = new System.Windows.Forms.NumericUpDown();
            this.numMin = new System.Windows.Forms.NumericUpDown();
            this.numSec = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numMilliSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSec)).BeginInit();
            this.SuspendLayout();
            // 
            // numMilliSeconds
            // 
            this.numMilliSeconds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numMilliSeconds.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numMilliSeconds.Location = new System.Drawing.Point(492, 5);
            this.numMilliSeconds.Margin = new System.Windows.Forms.Padding(8);
            this.numMilliSeconds.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numMilliSeconds.Name = "numMilliSeconds";
            this.numMilliSeconds.Size = new System.Drawing.Size(115, 38);
            this.numMilliSeconds.TabIndex = 5;
            this.numMilliSeconds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numMilliSeconds.ValueChanged += new System.EventHandler(this.numMilliSeconds_ValueChanged);
            // 
            // numHour
            // 
            this.numHour.Location = new System.Drawing.Point(0, 3);
            this.numHour.Name = "numHour";
            this.numHour.Size = new System.Drawing.Size(120, 42);
            this.numHour.TabIndex = 6;
            this.numHour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numMin
            // 
            this.numMin.Location = new System.Drawing.Point(163, 3);
            this.numMin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numMin.Name = "numMin";
            this.numMin.Size = new System.Drawing.Size(120, 42);
            this.numMin.TabIndex = 7;
            this.numMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numSec
            // 
            this.numSec.Location = new System.Drawing.Point(321, 3);
            this.numSec.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numSec.Name = "numSec";
            this.numSec.Size = new System.Drawing.Size(120, 42);
            this.numSec.TabIndex = 8;
            this.numSec.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(126, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 30);
            this.label1.TabIndex = 9;
            this.label1.Text = "H";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(289, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 30);
            this.label2.TabIndex = 10;
            this.label2.Text = "M";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(447, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 30);
            this.label3.TabIndex = 11;
            this.label3.Text = "S";
            // 
            // TimeSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numSec);
            this.Controls.Add(this.numMin);
            this.Controls.Add(this.numHour);
            this.Controls.Add(this.numMilliSeconds);
            this.Margin = new System.Windows.Forms.Padding(8);
            this.Name = "TimeSelector";
            this.Size = new System.Drawing.Size(614, 51);
            ((System.ComponentModel.ISupportInitialize)(this.numMilliSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NumericUpDown numMilliSeconds;
        private System.Windows.Forms.NumericUpDown numHour;
        private System.Windows.Forms.NumericUpDown numMin;
        private System.Windows.Forms.NumericUpDown numSec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}
