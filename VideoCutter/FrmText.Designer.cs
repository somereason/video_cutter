﻿namespace VideoCutter
{
    partial class FrmText
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCmd = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnSavebat = new System.Windows.Forms.Button();
            this.sfdSave = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCmd
            // 
            this.txtCmd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCmd.Location = new System.Drawing.Point(0, 0);
            this.txtCmd.Name = "txtCmd";
            this.txtCmd.Size = new System.Drawing.Size(905, 795);
            this.txtCmd.TabIndex = 0;
            this.txtCmd.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSavebat);
            this.panel1.Controls.Add(this.btnCopy);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 795);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(905, 100);
            this.panel1.TabIndex = 1;
            // 
            // btnCopy
            // 
            this.btnCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopy.Location = new System.Drawing.Point(498, 19);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(181, 59);
            this.btnCopy.TabIndex = 0;
            this.btnCopy.Text = "复制";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.BtnCopy_Click);
            // 
            // btnSavebat
            // 
            this.btnSavebat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSavebat.Location = new System.Drawing.Point(697, 19);
            this.btnSavebat.Name = "btnSavebat";
            this.btnSavebat.Size = new System.Drawing.Size(181, 59);
            this.btnSavebat.TabIndex = 1;
            this.btnSavebat.Text = "保存为bat";
            this.btnSavebat.UseVisualStyleBackColor = true;
            this.btnSavebat.Click += new System.EventHandler(this.BtnSavebat_Click);
            // 
            // sfdSave
            // 
            this.sfdSave.Filter = "*.bat|*.bat";
            // 
            // FrmText
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 895);
            this.Controls.Add(this.txtCmd);
            this.Controls.Add(this.panel1);
            this.Name = "FrmText";
            this.Text = "文本";
            this.Load += new System.EventHandler(this.FrmText_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtCmd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSavebat;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.SaveFileDialog sfdSave;
    }
}