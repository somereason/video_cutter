﻿namespace VideoCutter
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbPreview = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSetAsEndAll = new System.Windows.Forms.Button();
            this.btnSetAsStartAll = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbRealTimePreview = new System.Windows.Forms.CheckBox();
            this.btnSetAsEnd = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnSetAsStart = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSelFile = new System.Windows.Forms.Button();
            this.lbPath = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddToQueue = new System.Windows.Forms.Button();
            this.lbLength = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvMovieQueue = new System.Windows.Forms.DataGridView();
            this.colFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDurition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEdit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colDel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colReprocess = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnAddFolder = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnClearQueue = new System.Windows.Forms.Button();
            this.ofdSelMovie = new System.Windows.Forms.OpenFileDialog();
            this.tsTime = new VideoCutter.TimeSelector();
            this.tsEnd = new VideoCutter.TimeSelector();
            this.tsStart = new VideoCutter.TimeSelector();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovieQueue)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvMovieQueue);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(2000, 1462);
            this.splitContainer1.SplitterDistance = 1310;
            this.splitContainer1.SplitterWidth = 10;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbPreview);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.groupBox1.Size = new System.Drawing.Size(2000, 1022);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "预览";
            // 
            // pbPreview
            // 
            this.pbPreview.BackColor = System.Drawing.Color.Black;
            this.pbPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbPreview.Location = new System.Drawing.Point(8, 43);
            this.pbPreview.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.pbPreview.Name = "pbPreview";
            this.pbPreview.Size = new System.Drawing.Size(1984, 859);
            this.pbPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPreview.TabIndex = 0;
            this.pbPreview.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Honeydew;
            this.panel2.Controls.Add(this.btnSetAsEndAll);
            this.panel2.Controls.Add(this.btnSetAsStartAll);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.cbRealTimePreview);
            this.panel2.Controls.Add(this.tsTime);
            this.panel2.Controls.Add(this.btnSetAsEnd);
            this.panel2.Controls.Add(this.btnPlay);
            this.panel2.Controls.Add(this.btnSetAsStart);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(8, 902);
            this.panel2.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1984, 112);
            this.panel2.TabIndex = 1;
            // 
            // btnSetAsEndAll
            // 
            this.btnSetAsEndAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetAsEndAll.BackColor = System.Drawing.Color.Pink;
            this.btnSetAsEndAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetAsEndAll.Location = new System.Drawing.Point(1714, 32);
            this.btnSetAsEndAll.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnSetAsEndAll.Name = "btnSetAsEndAll";
            this.btnSetAsEndAll.Size = new System.Drawing.Size(242, 58);
            this.btnSetAsEndAll.TabIndex = 19;
            this.btnSetAsEndAll.Text = "设为所有结尾]";
            this.btnSetAsEndAll.UseVisualStyleBackColor = false;
            this.btnSetAsEndAll.Click += new System.EventHandler(this.btnSetAsEndAll_Click);
            // 
            // btnSetAsStartAll
            // 
            this.btnSetAsStartAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetAsStartAll.BackColor = System.Drawing.Color.Pink;
            this.btnSetAsStartAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetAsStartAll.Location = new System.Drawing.Point(1061, 32);
            this.btnSetAsStartAll.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnSetAsStartAll.Name = "btnSetAsStartAll";
            this.btnSetAsStartAll.Size = new System.Drawing.Size(242, 58);
            this.btnSetAsStartAll.TabIndex = 18;
            this.btnSetAsStartAll.Text = "[设为所有开头";
            this.btnSetAsStartAll.UseVisualStyleBackColor = false;
            this.btnSetAsStartAll.Click += new System.EventHandler(this.btnSetAsStartAll_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(48, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 30);
            this.label4.TabIndex = 17;
            this.label4.Text = "预览";
            // 
            // cbRealTimePreview
            // 
            this.cbRealTimePreview.AutoSize = true;
            this.cbRealTimePreview.Location = new System.Drawing.Point(139, 45);
            this.cbRealTimePreview.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.cbRealTimePreview.Name = "cbRealTimePreview";
            this.cbRealTimePreview.Size = new System.Drawing.Size(171, 34);
            this.cbRealTimePreview.TabIndex = 16;
            this.cbRealTimePreview.Text = "实时预览";
            this.cbRealTimePreview.UseVisualStyleBackColor = true;
            this.cbRealTimePreview.CheckedChanged += new System.EventHandler(this.cbRealTimePreview_CheckedChanged);
            // 
            // btnSetAsEnd
            // 
            this.btnSetAsEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetAsEnd.BackColor = System.Drawing.Color.GreenYellow;
            this.btnSetAsEnd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetAsEnd.Location = new System.Drawing.Point(1517, 32);
            this.btnSetAsEnd.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnSetAsEnd.Name = "btnSetAsEnd";
            this.btnSetAsEnd.Size = new System.Drawing.Size(190, 58);
            this.btnSetAsEnd.TabIndex = 10;
            this.btnSetAsEnd.Text = "设为结尾]";
            this.btnSetAsEnd.UseVisualStyleBackColor = false;
            this.btnSetAsEnd.Click += new System.EventHandler(this.btnSetAsEnd_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(937, 32);
            this.btnPlay.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(108, 58);
            this.btnPlay.TabIndex = 15;
            this.btnPlay.Text = "查看";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnSetAsStart
            // 
            this.btnSetAsStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetAsStart.BackColor = System.Drawing.Color.GreenYellow;
            this.btnSetAsStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetAsStart.Location = new System.Drawing.Point(1311, 32);
            this.btnSetAsStart.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnSetAsStart.Name = "btnSetAsStart";
            this.btnSetAsStart.Size = new System.Drawing.Size(198, 58);
            this.btnSetAsStart.TabIndex = 9;
            this.btnSetAsStart.Text = "[设为开头";
            this.btnSetAsStart.UseVisualStyleBackColor = false;
            this.btnSetAsStart.Click += new System.EventHandler(this.btnSetAsStart_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(0, 1022);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.groupBox2.Size = new System.Drawing.Size(2000, 288);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "编辑";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Linen;
            this.panel1.Controls.Add(this.btnSelFile);
            this.panel1.Controls.Add(this.lbPath);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtFileName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btnAddToQueue);
            this.panel1.Controls.Add(this.lbLength);
            this.panel1.Controls.Add(this.tsEnd);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tsStart);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(8, 43);
            this.panel1.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1984, 237);
            this.panel1.TabIndex = 17;
            // 
            // btnSelFile
            // 
            this.btnSelFile.Location = new System.Drawing.Point(192, 30);
            this.btnSelFile.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnSelFile.Name = "btnSelFile";
            this.btnSelFile.Size = new System.Drawing.Size(180, 58);
            this.btnSelFile.TabIndex = 21;
            this.btnSelFile.Text = "浏览...";
            this.btnSelFile.UseVisualStyleBackColor = true;
            this.btnSelFile.Click += new System.EventHandler(this.btnSelFile_Click);
            // 
            // lbPath
            // 
            this.lbPath.AutoSize = true;
            this.lbPath.Location = new System.Drawing.Point(188, 108);
            this.lbPath.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(28, 30);
            this.lbPath.TabIndex = 20;
            this.lbPath.Text = "\"";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(48, 102);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 30);
            this.label5.TabIndex = 19;
            this.label5.Text = "路径";
            // 
            // txtFileName
            // 
            this.txtFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFileName.Enabled = false;
            this.txtFileName.Location = new System.Drawing.Point(380, 32);
            this.txtFileName.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(1565, 42);
            this.txtFileName.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1052, 168);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 30);
            this.label3.TabIndex = 17;
            this.label3.Text = "----";
            // 
            // btnAddToQueue
            // 
            this.btnAddToQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddToQueue.Location = new System.Drawing.Point(1769, 155);
            this.btnAddToQueue.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnAddToQueue.Name = "btnAddToQueue";
            this.btnAddToQueue.Size = new System.Drawing.Size(188, 58);
            this.btnAddToQueue.TabIndex = 4;
            this.btnAddToQueue.Text = "加入队列";
            this.btnAddToQueue.UseVisualStyleBackColor = true;
            this.btnAddToQueue.Click += new System.EventHandler(this.btnAddToQueue_Click);
            // 
            // lbLength
            // 
            this.lbLength.AutoSize = true;
            this.lbLength.Location = new System.Drawing.Point(188, 168);
            this.lbLength.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbLength.Name = "lbLength";
            this.lbLength.Size = new System.Drawing.Size(28, 30);
            this.lbLength.TabIndex = 7;
            this.lbLength.Text = "\"";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 165);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 30);
            this.label2.TabIndex = 6;
            this.label2.Text = "长度";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 30);
            this.label1.TabIndex = 5;
            this.label1.Text = "文件名";
            // 
            // dgvMovieQueue
            // 
            this.dgvMovieQueue.AllowUserToAddRows = false;
            this.dgvMovieQueue.AllowUserToDeleteRows = false;
            this.dgvMovieQueue.AllowUserToResizeColumns = false;
            this.dgvMovieQueue.AllowUserToResizeRows = false;
            this.dgvMovieQueue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMovieQueue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFileName,
            this.colStatus,
            this.colDurition,
            this.colStart,
            this.colEnd,
            this.colEdit,
            this.colDel,
            this.colReprocess});
            this.dgvMovieQueue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMovieQueue.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvMovieQueue.Location = new System.Drawing.Point(0, 0);
            this.dgvMovieQueue.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.dgvMovieQueue.MultiSelect = false;
            this.dgvMovieQueue.Name = "dgvMovieQueue";
            this.dgvMovieQueue.ReadOnly = true;
            this.dgvMovieQueue.RowHeadersVisible = false;
            this.dgvMovieQueue.RowHeadersWidth = 102;
            this.dgvMovieQueue.RowTemplate.Height = 23;
            this.dgvMovieQueue.Size = new System.Drawing.Size(1788, 142);
            this.dgvMovieQueue.TabIndex = 0;
            this.dgvMovieQueue.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMovieQueue_CellContentClick);
            this.dgvMovieQueue.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvMovieQueue_CellFormatting);
            // 
            // colFileName
            // 
            this.colFileName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colFileName.DataPropertyName = "FileName";
            this.colFileName.HeaderText = "文件名";
            this.colFileName.MinimumWidth = 12;
            this.colFileName.Name = "colFileName";
            this.colFileName.ReadOnly = true;
            // 
            // colStatus
            // 
            this.colStatus.DataPropertyName = "status";
            this.colStatus.HeaderText = "状态";
            this.colStatus.MinimumWidth = 12;
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            this.colStatus.Width = 250;
            // 
            // colDurition
            // 
            this.colDurition.DataPropertyName = "Duration";
            this.colDurition.HeaderText = "总时长";
            this.colDurition.MinimumWidth = 12;
            this.colDurition.Name = "colDurition";
            this.colDurition.ReadOnly = true;
            this.colDurition.Width = 250;
            // 
            // colStart
            // 
            this.colStart.DataPropertyName = "Start";
            this.colStart.HeaderText = "开始";
            this.colStart.MinimumWidth = 12;
            this.colStart.Name = "colStart";
            this.colStart.ReadOnly = true;
            this.colStart.Width = 250;
            // 
            // colEnd
            // 
            this.colEnd.DataPropertyName = "End";
            this.colEnd.HeaderText = "结束";
            this.colEnd.MinimumWidth = 12;
            this.colEnd.Name = "colEnd";
            this.colEnd.ReadOnly = true;
            this.colEnd.Width = 250;
            // 
            // colEdit
            // 
            this.colEdit.HeaderText = "编辑";
            this.colEdit.MinimumWidth = 12;
            this.colEdit.Name = "colEdit";
            this.colEdit.ReadOnly = true;
            this.colEdit.Text = "编辑";
            this.colEdit.UseColumnTextForButtonValue = true;
            this.colEdit.Width = 60;
            // 
            // colDel
            // 
            this.colDel.HeaderText = "删除";
            this.colDel.MinimumWidth = 12;
            this.colDel.Name = "colDel";
            this.colDel.ReadOnly = true;
            this.colDel.Text = "删除";
            this.colDel.UseColumnTextForButtonValue = true;
            this.colDel.Width = 60;
            // 
            // colReprocess
            // 
            this.colReprocess.HeaderText = "再次处理";
            this.colReprocess.MinimumWidth = 12;
            this.colReprocess.Name = "colReprocess";
            this.colReprocess.ReadOnly = true;
            this.colReprocess.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colReprocess.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colReprocess.Text = "再次处理";
            this.colReprocess.UseColumnTextForButtonValue = true;
            this.colReprocess.Width = 80;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnStart);
            this.panel3.Controls.Add(this.btnAddFolder);
            this.panel3.Controls.Add(this.btnAbout);
            this.panel3.Controls.Add(this.btnClearQueue);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1788, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(212, 142);
            this.panel3.TabIndex = 5;
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.Pink;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Location = new System.Drawing.Point(12, 8);
            this.btnStart.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(188, 98);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "开始";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnAddFolder
            // 
            this.btnAddFolder.Location = new System.Drawing.Point(12, 120);
            this.btnAddFolder.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnAddFolder.Name = "btnAddFolder";
            this.btnAddFolder.Size = new System.Drawing.Size(188, 68);
            this.btnAddFolder.TabIndex = 4;
            this.btnAddFolder.Text = "快速添加";
            this.btnAddFolder.UseVisualStyleBackColor = true;
            this.btnAddFolder.Click += new System.EventHandler(this.btnAddFolder_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.Location = new System.Drawing.Point(12, 278);
            this.btnAbout.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(188, 70);
            this.btnAbout.TabIndex = 2;
            this.btnAbout.Text = "关于";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnClearQueue
            // 
            this.btnClearQueue.Location = new System.Drawing.Point(12, 195);
            this.btnClearQueue.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.btnClearQueue.Name = "btnClearQueue";
            this.btnClearQueue.Size = new System.Drawing.Size(188, 68);
            this.btnClearQueue.TabIndex = 3;
            this.btnClearQueue.Text = "清空";
            this.btnClearQueue.UseVisualStyleBackColor = true;
            this.btnClearQueue.Click += new System.EventHandler(this.btnClearQueue_Click);
            // 
            // ofdSelMovie
            // 
            this.ofdSelMovie.Filter = "视频|*.mkv;*.mp4;*.wmv;*.flv;*.avi;*.mov;";
            // 
            // tsTime
            // 
            this.tsTime.Location = new System.Drawing.Point(309, 38);
            this.tsTime.Margin = new System.Windows.Forms.Padding(20);
            this.tsTime.Name = "tsTime";
            this.tsTime.Size = new System.Drawing.Size(628, 52);
            this.tsTime.TabIndex = 11;
            this.tsTime.Time = System.TimeSpan.Parse("00:00:00");
            // 
            // tsEnd
            // 
            this.tsEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tsEnd.Location = new System.Drawing.Point(1136, 158);
            this.tsEnd.Margin = new System.Windows.Forms.Padding(20);
            this.tsEnd.Name = "tsEnd";
            this.tsEnd.Size = new System.Drawing.Size(619, 52);
            this.tsEnd.TabIndex = 13;
            this.tsEnd.Time = System.TimeSpan.Parse("00:00:00");
            // 
            // tsStart
            // 
            this.tsStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tsStart.Location = new System.Drawing.Point(430, 158);
            this.tsStart.Margin = new System.Windows.Forms.Padding(20);
            this.tsStart.Name = "tsStart";
            this.tsStart.Size = new System.Drawing.Size(625, 52);
            this.tsStart.TabIndex = 12;
            this.tsStart.Time = System.TimeSpan.Parse("00:00:00");
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2000, 1462);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.Name = "MainForm";
            this.Text = "视频截取工具";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovieQueue)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pbPreview;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.DataGridView dgvMovieQueue;
        private System.Windows.Forms.Button btnSetAsEnd;
        private System.Windows.Forms.Button btnSetAsStart;
        private System.Windows.Forms.Label lbLength;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddToQueue;
        private TimeSelector tsEnd;
        private TimeSelector tsStart;
        private TimeSelector tsTime;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.OpenFileDialog ofdSelMovie;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.CheckBox cbRealTimePreview;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnClearQueue;
        private System.Windows.Forms.Button btnSetAsEndAll;
        private System.Windows.Forms.Button btnSetAsStartAll;
        private System.Windows.Forms.Button btnSelFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDurition;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEnd;
        private System.Windows.Forms.DataGridViewButtonColumn colEdit;
        private System.Windows.Forms.DataGridViewButtonColumn colDel;
        private System.Windows.Forms.DataGridViewButtonColumn colReprocess;
        private System.Windows.Forms.Button btnAddFolder;
        private System.Windows.Forms.Panel panel3;
    }
}

