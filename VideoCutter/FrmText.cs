﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoCutter
{
    public partial class FrmText : Form
    {
        public FrmText()
        {
            InitializeComponent();
        }
        public FrmText(string txt)
        {
            InitializeComponent();
            txtCmd.Text = txt;
        }

        private void FrmText_Load(object sender, EventArgs e)
        {

        }

        private void BtnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtCmd.Text);
        }

        private void BtnSavebat_Click(object sender, EventArgs e)
        {
            if (sfdSave.ShowDialog() == DialogResult.OK)
            {
                System.IO.File.WriteAllText(sfdSave.FileName, txtCmd.Text);
            }
        }
    }
}
