﻿using System;
using System.IO;
namespace VideoCutter
{
    enum MovieFileStatus
    {
        Waiting,
        Processing,
        Finished,
    }
    class MovieFile
    {
        public string FileName { get; set; } = "";
        private TimeSpan _Start = AppVars.emptyTimeSpan;
        public TimeSpan Start
        {
            get { return _Start; }
            set
            {
                if (value < AppVars.emptyTimeSpan)
                    _Start = AppVars.emptyTimeSpan;
                else
                    _Start = (value < Duration) ? value : Duration;
            }
        }
        private TimeSpan _End = AppVars.emptyTimeSpan;
        public TimeSpan End
        {
            get { return _End; }
            set
            {
                if (value < AppVars.emptyTimeSpan)
                    _End = AppVars.emptyTimeSpan;
                else
                    _End = (value < Duration) ? value : Duration;
            }
        }

        private string _DurationStr { get; set; } = "00:00:00.00";
        public MovieFileStatus status { get; set; } = MovieFileStatus.Waiting;
        /// <summary>
        /// 所有关于时间的属性,一定要先设置这个.
        /// </summary>
        public string DurationStr
        {
            get
            { return _DurationStr; }
            set
            {
                _DurationStr = value;
                Duration = Helper.StringHelper.Str2TimeSpan(value);
            }
        }


        public TimeSpan Duration { get; set; } = AppVars.emptyTimeSpan;

        string _FilePath = "";
        public string FilePath
        {
            get { return this._FilePath; }
            set
            {
                this._FilePath = value;
                this.FileName = Path.GetFileName(value);
            }
        }


        public static MovieFile getNew(String path)
        {
            var ret = new MovieFile();
            ret.FilePath = path;
            ret.DurationStr = Helper.VideoHelper.GetVideoDurationStr(ret.FilePath);
            ret.End = ret.Duration;
            return ret;
        }
    }
}
