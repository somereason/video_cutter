﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VideoCutter.Helper
{
    class StringHelper
    {
        public static string TimeSpan2Str(TimeSpan t)
        {
            return string.Format("{0}:{1}:{2}.{3}", t.Hours, t.Minutes, t.Seconds, t.Milliseconds / 10);
        }

        public static TimeSpan Str2TimeSpan(string t)
        {
            Regex regex = new Regex(@"^(\d+)\:(\d+)\:(\d+)\.(\d+)$");
            Match m = regex.Match(t);
            if (m.Success)
            {
                return new TimeSpan(0
                       , int.Parse(m.Groups[1].Value)
                       , int.Parse(m.Groups[2].Value)
                       , int.Parse(m.Groups[3].Value)
                       , int.Parse(m.Groups[4].Value) * 10);
            }
            else
                return AppVars.emptyTimeSpan;
        }
    }
}
