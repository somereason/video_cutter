﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VideoCutter.Helper
{
    class AppHelper
    {
        public static string GetResourcePath(string path)
        {
            return Path.Combine(AppVars.appPath, path);
        }
    }
}
