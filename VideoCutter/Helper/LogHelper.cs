﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace VideoCutter.Helper
{
    class LogHelper
    {
        static readonly string LOG_FILE_NAME = "log.txt";

        public static void WriteLog(LogType logType, String content)
        {
            using (TextWriter sw = new StreamWriter(AppHelper.GetResourcePath(LOG_FILE_NAME), true, Encoding.UTF8))
            {
                sw.WriteLine();
                sw.WriteLine(string.Format("=========={0}======{1}============", logType.ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                sw.WriteLine(content);
                sw.WriteLine();
            }
        }
    }

    enum LogType
    {
        ffmpeg_command,
        ffmpeg_info,
        ffmpeg_error,
        general_error,

    }
}
